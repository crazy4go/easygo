package main

import (
	"gitee.com/crazy4go/easygo/log"
)

func main() {
	log.OpenTrace()
	// log.CloseTrace()
	defer log.Close()
	// log.SetLogToFile("mytest", "localhost", "./logs", 1024, 3, log.DEBUG)
	log.Error("error msg")
}
