package prcid

// 中华人民共和国居民身份证有效性校验
import (
	"fmt"
	"math"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gitee.com/crazy4go/easygo/str"
)

// 计算规则参考“中国国家标准化管理委员会”官方文档：http://www.gb688.cn/bzgk/gb/newGbInfo?hcno=080D6FBF2BB468F9007657F26D60013E
// 标准号：GB 11643-1999：公民身份证号码

// IsPRCID 判断是否是合法的中华人民共和国居民身份证
func IsPRCID(idNum string) bool {
	//a1与对应的校验码对照表，其中key表示a1，value表示校验码，value中的10表示校验码X
	var a1Map = map[int]int{
		0:  1,
		1:  0,
		2:  10,
		3:  9,
		4:  8,
		5:  7,
		6:  6,
		7:  5,
		8:  4,
		9:  3,
		10: 2,
	}

	var idStr = strings.ToUpper(idNum)
	var reg, err = regexp.Compile(`^[0-9]{17}[0-9X]$`)
	if err != nil {
		return false
	}
	if !reg.Match([]byte(idStr)) {
		return false
	}
	var sum int
	var signChar = ""
	for index, c := range idStr {
		var i = 18 - index
		if i != 1 {
			if v, err := strconv.Atoi(string(c)); err == nil {
				//计算加权因子
				var weight = int(math.Pow(2, float64(i-1))) % 11
				sum += v * weight
			} else {
				return false
			}
		} else {
			signChar = string(c)
		}
	}
	var a1 = a1Map[sum%11]
	var a1Str = fmt.Sprintf("%d", a1)
	if a1 == 10 {
		a1Str = "X"
	}
	if a1Str == signChar {
		return true
	}
	return false
}

// Birthday 从身份证中获取出生日期
func Birthday(idNum string) (date time.Time, err error) {
	if !IsPRCID(idNum) {
		err = fmt.Errorf("身份证号 %s 非法", idNum)
		return
	}
	var yearStr = str.SubStr(idNum, 6, 4)
	var monthStr = str.SubStr(idNum, 10, 2)
	var dayStr = str.SubStr(idNum, 12, 2)
	year, err := strconv.Atoi(yearStr)
	if err != nil {
		return
	}
	month, err := strconv.Atoi(monthStr)
	if err != nil {
		return
	}
	day, err := strconv.Atoi(dayStr)
	if err != nil {
		return
	}
	date = time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local)
	return
}

// Gender 从身份证号中获取性别,F 表示女性,M 表示男性
func Gender(idNum string) (gender string, err error) {
	if !IsPRCID(idNum) {
		err = fmt.Errorf("身份证号 %s 非法", idNum)
		return
	}
	numStr := str.SubStr(idNum, 14, 3)
	num, err := strconv.Atoi(numStr)
	if err != nil {
		return
	}
	if num%2 == 0 {
		gender = "F"
	} else {
		gender = "M"
	}
	return
}
