package str

import (
	"encoding/json"
	"regexp"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"

	"gitee.com/crazy4go/easygo/log"
)

// TrimAll 去掉字符串首尾空格和其中的回车换行符
// 传入字符串指针,不需要返回值
func TrimAll(s ...*string) {
	for i := 0; i < len(s); i++ {
		*s[i] = strings.TrimSpace(*s[i])
		*s[i] = strings.ReplaceAll(*s[i], "\r", "")
		*s[i] = strings.ReplaceAll(*s[i], "\n", "")
	}
}

// TrimJSONErrorChars 去掉可能导致JSON解析错误的字符
func TrimJSONErrorChars(s string) string {
	b := []byte(s)
	for i, ch := range b {

		switch {
		case ch > '~':
			b[i] = ' '
		case ch == '\r':
		case ch == '\n':
		case ch == '\t':
		case ch < ' ':
			b[i] = ' '
		}
	}
	return string(b)
}

// SubStr 字符串截取,从begin位开始,取长度为length的子串
func SubStr(str string, begin, length int) string {
	rs := []rune(str)
	lth := len(rs)
	if begin < 0 {
		begin = 0
	}
	if begin >= lth {
		begin = lth
	}
	end := begin + length
	if end > lth {
		end = lth
	}
	return string(rs[begin:end])
}

// ToInt64 字符串转10进制int64
func ToInt64(str string) (value int64, err error) {
	value, err = strconv.ParseInt(str, 10, 64)
	return
}

// IncludeCN 检查字符串中是否存在汉字字符
func IncludeCN(str string) bool {
	for _, r := range str {
		if unicode.Is(unicode.Scripts["Han"], r) || (regexp.MustCompile("[\u3002\uff1b\uff0c\uff1a\u201c\u201d\uff08\uff09\u3001\uff1f\u300a\u300b]").MatchString(string(r))) {
			return true
		}
	}
	return false
}

// Length 检查字符串长度,如果包含汉字,一个汉字长度为 1
func Length(str string) int {
	return utf8.RuneCountInString(str)
}

// ToJSON 把对象转换为字符串
func ToJSON(obj interface{}) (str string) {
	if bt, err := json.Marshal(obj); err != nil {
		log.Errorf("StructToJson-转Json:Struct=%v,Err=%v", obj, err)
	} else {
		str = string(bt)
	}
	return
}
