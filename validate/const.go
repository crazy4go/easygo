package validate

// 常量定义
const (
	// 正则定义
	regexEmail = `^[0-9a-z][_.0-9a-z-]{0,31}@([0-9a-z][0-9a-z-]{0,30}[0-9a-z]\.){1,4}[a-z]{2,4}$`
	regexPhone = "^((13[0-9])|(14[5,7])|(15[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|166|198|199|(147))\\d{8}$"
	// tag 属性定义
	tagName        = "val"      // tag 名称
	tagSplit       = ","        // tag 分割符号
	tagAssign      = "="        // tag 赋值符号
	tagParamSplit  = ":"        // tag 参数间隔
	tabValidateFun = "Validate" // tag 校验器的校验方法名
	// 错误定义
	errEmptyStruct   = "结构体 %v 为空"                          // 结构体为空
	errMust          = "字段 %v 为必填项"                         // 字段项必填
	errLengthLT      = "字段 %v 的长度不能小于 %v"                   // 长度校验,太短
	errLengthGT      = "字段 %v 的长度不能大于 %v"                   // 长度校验,太长
	errUnsupportType = "不支持的校验类型,当前版本仅支持[string,int,float]" // 不支持的校验类型
	errTagFormat     = "标记 %v 未定义"                          // 不支持的校验标记
	errNotStruct     = "%v 不是一个Struct"                      // 不是一个struct
	errEmail         = "字段 %v 的值 %v 不是有效的电子邮箱地址"            // 电子邮箱地址错误
	errMPhone        = "字段 %v 的值 %v 不是有效的手机号"               // 手机号错误
	errMin           = "字段 %v 的值不能小于 %v,当前值 %v"             // 最小值错误
	errMax           = "字段 %v 的值不能大于 %v,当前值 %v"             // 最大值错误
)
