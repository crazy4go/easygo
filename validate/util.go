package validate

import (
	"fmt"
	"regexp"

	"gitee.com/crazy4go/easygo/prcid"
	"gitee.com/crazy4go/easygo/usci"
)

// CheckPhone 判断手机号是否合法
func CheckPhone(phone string) (ok bool) {
	ok = regexp.MustCompile(regexPhone).MatchString(phone)
	return
}

// CheckEmail 判断邮箱地址是否合法
func CheckEmail(email string) (ok bool) {
	ok = regexp.MustCompile(regexEmail).MatchString(email)
	return
}

// CheckPassword 检查密码强度是否合法
// 1:密码长度6-24位
// 2:密码长度6-24位,必须同时包含字母和数字
// 3:密码长度6-24位,必须同时包含大写字母,小写字母和数字
// 4:密码长度6-24位,必须同时包含大写字母,小写字母,数字和特殊字符[!@#~$%^&*()+|_]
func CheckPassword(pwd string, level int) (err error) {
	if level < 1 || level > 4 {
		err = fmt.Errorf("密码强度级别%v非法,可选级别为[1-4]", level)
		return
	}
	if len(pwd) < 6 || len(pwd) > 24 {
		err = fmt.Errorf("密码长度必须为[6-24]个字符")
		return
	}
	num := `[0-9]{1}`
	az := `[a-z]{1}`
	AZ := `[A-Z]{1}`
	symbol := `[!@#~$%^&*()+|_]{1}`
	var b0, b1, b2, b3 bool
	switch level {
	case 2: // 字母数字组合
		b0, _ = regexp.MatchString(num, pwd)
		b1, _ = regexp.MatchString(az, pwd)
		b2, _ = regexp.MatchString(AZ, pwd)
		if !b0 || (!b1 && !b2) {
			err = fmt.Errorf("密码要求同时包含至少一个字母和数字")
			return
		}
	case 3: // 大小写字母数字组合
		b0, _ = regexp.MatchString(num, pwd)
		b1, _ = regexp.MatchString(az, pwd)
		b2, _ = regexp.MatchString(AZ, pwd)
		if !b0 || !b1 || !b2 {
			err = fmt.Errorf("密码要求同时包含至少一个大写字母,小写字母和数字")
			return
		}
	case 4: // 必须包含数字,大写字母,小写字母,特殊字符[!@#~$%^&*()+|_]
		b0, _ = regexp.MatchString(num, pwd)
		b1, _ = regexp.MatchString(az, pwd)
		b2, _ = regexp.MatchString(AZ, pwd)
		b3, _ = regexp.MatchString(symbol, pwd)
		if !b0 || !b1 || !b2 || !b3 {
			err = fmt.Errorf("密码要求同时包含至少一个大写字母,小写字母,数字和特殊字符")
			return
		}
	}
	return
}

// CheckPRCID 检查身份证号是否合法
func CheckPRCID(id string) (ok bool) {
	ok = prcid.IsPRCID(id)
	return
}

// CheckUSCI 检查统一社会信用代码是否合法
func CheckUSCI(id string) (ok bool) {
	ok = usci.IsUSCI(id)
	return
}
