package validate

import (
	"fmt"
	"reflect"
	"strconv"
	"unicode/utf8"
)

// Length 对 string 进行长度校验
// 举例 len=1:10
type Length struct{}

// Validate 校验实现
func (s *Length) Validate(t reflect.StructField, v reflect.Value, params []string) (err error) {
	if t.Type.Kind() != reflect.String {
		err = fmt.Errorf("[Length.Valudate()]不支持%s类型的参数,可用于string", t.Type)
		return
	}
	paramCount := len(params)
	if paramCount < 1 { // 没有参数,报错返回
		err = fmt.Errorf("[Length.Valudate()]未指定长度参数")
		return
	}
	var min, max int64
	// 取第一个参数赋值给min
	if min, err = string2int64(params[0]); err != nil {
		return
	}
	// 如果有第二个参数,把第二个赋值给max
	if paramCount > 1 { // 大于一个参数,取前两个参数,长度必须介于这两个参数之间
		if max, err = string2int64(params[1]); err != nil {
			return
		}
	} else { // 如果没有第二个参数,把min赋值给max
		max = min
	}
	if max < min {
		max, min = min, max
	}
	len := utf8.RuneCountInString(v.String())
	len64 := int64(len)
	if len64 < min {
		err = fmt.Errorf(errLengthLT, t.Name, min)
	}
	if len64 > max {
		err = fmt.Errorf(errLengthGT, t.Name, max)
	}
	return
}

// 把字符串转为int64
func string2int64(str string) (v int64, err error) {
	if v, err = strconv.ParseInt(str, 10, 64); err != nil {
		err = fmt.Errorf("[Length.Valudate()]%s无法转换为整数:%v", str, err)
	}
	return
}

// In 对 int 和 string 进行枚举集合校验
type In struct{}

// Validate 校验实现
func (s *In) Validate(t reflect.StructField, v reflect.Value, params []string) (err error) {
	kind := t.Type.Kind()
	for i := 0; i < len(params); i++ {
		switch kind {
		case reflect.String:
			if v.String() == params[i] {
				return
			}
		case reflect.Int:
			var p64 int64
			if p64, err = strconv.ParseInt(params[i], 10, 64); err != nil {
				err = fmt.Errorf("[In.Validate()]%s不能转换为int类型", params[i])
				return
			}
			if p64 == v.Int() {
				return
			}
		default:
			err = fmt.Errorf("[In.Validate()]不能用于%s类型,可用于string和int", t.Type.String())
			return
		}
	}
	err = fmt.Errorf("[In.Validate()]字段%v的值[%v]未包含于%v", t.Name, v, params)
	return
}

// Bool 对 string 进行合法性
type Bool struct{}

// Validate 校验实现
func (s *Bool) Validate(t reflect.StructField, v reflect.Value, params []string) (err error) {
	switch t.Type.Kind() {
	case reflect.String:
		if v.String() != "Y" && v.String() != "N" {
			err = fmt.Errorf("字段%v的值%v不是有效的布尔值", t.Name, v)
		}
	default:
		err = fmt.Errorf("布尔校验只适用于string类型,不能用于%s类型", t.Type.String())
		return
	}
	return
}

// Must 对字段进行非空检查
type Must struct{}

// Validate 校验实现
func (s *Must) Validate(t reflect.StructField, v reflect.Value, params []string) (err error) {
	if isEmptyValue(v) {
		err = fmt.Errorf(errMust, t.Name)
	}
	return
}

// Email 对字段进行非空检查
type Email struct{}

// Validate 校验实现
func (s *Email) Validate(t reflect.StructField, v reflect.Value, params []string) (err error) {
	if t.Type.Kind() != reflect.String {
		err = fmt.Errorf("邮箱地址校验只适用于string类型,不能用于%v类型", t.Name)
		return
	}
	if !CheckEmail(v.String()) {
		err = fmt.Errorf(errEmail, t.Name, v)
	}
	return
}

// MobilePhone 对字段进行非空检查
type MobilePhone struct{}

// Validate 校验实现
func (s *MobilePhone) Validate(t reflect.StructField, v reflect.Value, params []string) (err error) {
	if t.Type.Kind() != reflect.String {
		err = fmt.Errorf("手机号校验只适用于string类型,不能用于%v类型", t.Name)
		return
	}
	if !CheckPhone(v.String()) {
		err = fmt.Errorf(errMPhone, t.Name, v)
	}
	return
}

// Min 对字段进行最小值校验
type Min struct{}

// Validate 校验实现
func (s *Min) Validate(t reflect.StructField, v reflect.Value, params []string) (err error) {
	paramCount := len(params)
	if paramCount < 1 { // 没有参数,报错返回
		err = fmt.Errorf("最小值校验至少需要一个数字参数")
		return
	}
	switch t.Type.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		var min int64
		min, err = strconv.ParseInt(params[0], 10, 64)
		if err != nil {
			err = fmt.Errorf("参数%s无法转换为整数", params[0])
			return
		}
		if v.Int() < min {
			err = fmt.Errorf(errMin, t.Name, min, v)
			return
		}
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		var min uint64
		min, err = strconv.ParseUint(params[0], 10, 64)
		if err != nil {
			err = fmt.Errorf("参数%s无法转换为无符号整数", params[0])
			return
		}
		if v.Uint() < min {
			err = fmt.Errorf(errMin, t.Name, min, v)
			return
		}
	case reflect.Float32, reflect.Float64:
		var min float64
		min, err = strconv.ParseFloat(params[0], 64)
		if err != nil {
			err = fmt.Errorf("参数%s无法转换为浮点数", params[0])
			return
		}
		if v.Float() < min {
			err = fmt.Errorf(errMin, t.Name, min, v)
			return
		}
	default:
		err = fmt.Errorf("最小值校验只适用于float和int类型,不能用于%s类型", t.Type.String())
	}
	return
}

// Max 对字段进行最大值校验
type Max struct{}

// Validate 校验实现
func (s *Max) Validate(t reflect.StructField, v reflect.Value, params []string) (err error) {
	paramCount := len(params)
	if paramCount < 1 { // 没有参数,报错返回
		err = fmt.Errorf("最小值校验至少需要一个数字参数")
		return
	}
	switch t.Type.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		var max int64
		max, err = strconv.ParseInt(params[0], 10, 64)
		if err != nil {
			err = fmt.Errorf("参数%s无法转换为整数", params[0])
			return
		}
		if v.Int() > max {
			err = fmt.Errorf(errMax, t.Name, max, v)
			return
		}
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		var max uint64
		max, err = strconv.ParseUint(params[0], 10, 64)
		if err != nil {
			err = fmt.Errorf("参数%s无法转换为无符号整数", params[0])
			return
		}
		if v.Uint() > max {
			err = fmt.Errorf(errMax, t.Name, max, v)
			return
		}
	case reflect.Float32, reflect.Float64:
		var max float64
		max, err = strconv.ParseFloat(params[0], 64)
		if err != nil {
			err = fmt.Errorf("参数%s无法转换为浮点数", params[0])
			return
		}
		if v.Float() > max {
			err = fmt.Errorf(errMax, t.Name, max, v)
			return
		}
	default:
		err = fmt.Errorf("最小值校验只适用于float和int类型,不能用于%s类型", t.Type.String())
	}
	return
}
