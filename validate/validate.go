package validate

import (
	"fmt"
	"reflect"
	"strings"
)

var validatorMap = map[string]interface{}{
	"len":    &Length{},
	"in":     &In{},
	"bool":   &Bool{},
	"must":   &Must{},
	"email":  &Email{},
	"mphone": &MobilePhone{},
	"min":    &Min{},
	"max":    &Max{},
}

// CheckStruct 校验结构体
func CheckStruct(objs ...interface{}) (err error) {
	for _, obj := range objs {
		rt := reflect.TypeOf(obj)
		rv := reflect.ValueOf(obj)
		if rt.Kind() == reflect.Ptr {
			rt = rt.Elem()
			rv = rv.Elem()
		}
		switch rt.Kind() {
		case reflect.Struct:
			// 开始校验结构体每个字段
			numFields := rv.NumField()
			if numFields <= 0 { // 值字段数量为 0
				err = fmt.Errorf(errEmptyStruct, rt.Name())
				return
			}
			for i := 0; i < numFields; i++ {
				fieldVInfo := rv.Field(i)
				fieldTInfo := rt.Field(i)
				fieldType := fieldTInfo.Type.Kind()
				tagString := fieldTInfo.Tag.Get(tagName)
				if tagString != "" {
					switch fieldType {
					case reflect.String, reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64, reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr, reflect.Float32, reflect.Float64:
						if !strings.Contains(tagString, "must") && isEmptyValue(fieldVInfo) {
							continue
						}
						if err = validateFromTag(tagString, fieldTInfo, fieldVInfo); err != nil { // 根据Tag进行校验
							return // 有错误就返回,每次只返回一个错误,注意这里不是break
						}
					default: // 只对string和数字类进行验证,其余类型暂不支持
						err = fmt.Errorf(errUnsupportType)
						break
					}

				}
			}
		default:
			err = fmt.Errorf(errNotStruct, rt.Name())
		}
	}
	return
}

// 根据tag进行数据校验
func validateFromTag(tagString string, fieldType reflect.StructField, fieldValue reflect.Value) (err error) {
	tags := strings.Split(tagString, tagSplit) // 把tagString用 tagSplit 拆分为多个独立tag
	var tagName string
	for i := 0; i < len(tags); i++ { // 对每个独立的tag进行处理
		var tagParam []string
		indexAssign := strings.Index(tags[i], tagAssign) // 检查tag中是否包含赋值字符 tagAssign,以拆分出tag的name和param
		if indexAssign == -1 {                           // 不存在赋值字符TagAssign,tagName = tags[i]
			tagName = strings.TrimSpace(tags[i]) // 去除两侧空格
		} else { // 存在赋值字符 TagAssign,tagName = tags[i][:indexAssign]
			tagName = strings.TrimSpace(tags[i][:indexAssign])
			paramString := tags[i][indexAssign+1:]                  // 获取tag参数进行处理
			paramSplit := strings.Index(paramString, tagParamSplit) // 检查参数有没有参数分割符 tagParamSplit
			if paramSplit == -1 {                                   // 没有参数分割符
				tagParam = append(tagParam, strings.TrimSpace(paramString)) // 直接把 paramString 赋值给 tagParam[0]
			} else { // 含有参数分割符
				for _, v := range strings.Split(paramString, tagParamSplit) { // 把参数分割后分别赋值给 tagParam
					tagParam = append(tagParam, strings.TrimSpace(v))
				}
			}
		}
		if err = call(fieldType, fieldValue, strings.ToLower(tagName), tagParam); err != nil {
			return
		}
	}
	return
}

func call(t reflect.StructField, v reflect.Value, tag string, params []string) (err error) {
	validator, ok := validatorMap[tag]
	if !ok {
		err = fmt.Errorf(errTagFormat, tag)
		return
	}
	mv := reflect.ValueOf(validator).MethodByName(tabValidateFun)
	args := []reflect.Value{reflect.ValueOf(t), reflect.ValueOf(v), reflect.ValueOf(params)}
	ret := mv.Call(args)
	if ret[0].Interface() != nil { // TODO 这种返回方式有点怪异,可以考虑下如何修改
		err = ret[0].Interface().(error)
	}
	return
}

// 判断是否是空值
func isEmptyValue(val reflect.Value) bool {
	typeKind := val.Kind()
	switch typeKind {
	case reflect.String, reflect.Array:
		return val.Len() == 0
	case reflect.Map, reflect.Slice:
		return val.Len() == 0 || val.IsNil()
	case reflect.Bool:
		return !val.Bool()
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return val.Int() == 0
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return val.Uint() == 0
	case reflect.Float32, reflect.Float64:
		return val.Float() == 0
	case reflect.Interface, reflect.Ptr:
		return val.IsNil()
	}
	return reflect.DeepEqual(val.Interface(), reflect.Zero(val.Type()).Interface())
}
