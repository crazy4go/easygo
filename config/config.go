//Package config CopyRight:2020
//作者:执着是因为无知
//配置格式为  key = value 如果有单双引号会被当作值处理,区分大小写
//支持用 '#' 或 '//' 作为注释符
//支持用 {{VAR}} 在每行设置一个变量,每行最多允许一个变量
package config

import (
	"bufio"
	"errors"
	"io"
	"os"
	"strings"
)

//变量分割符定义
const (
	varPrefix = "{{"
	varSuffix = "}}"
)

//Load 加载配置文件返回kv键值对
func Load(path string) (kv map[string]string, err error) {
	kv = make(map[string]string)
	var f *os.File
	defer f.Close()
	if f, err = os.Open(path); err != nil {
		return
	}
	//开始读取配置文件
	r := bufio.NewReader(f)
	for {
		var line []byte
		var isPrefix bool
		line, isPrefix, err = r.ReadLine()
		//ReadLine方法不能一次读取完整的一行,程序退出
		if isPrefix {
			err = errors.New("配置文件单行长度超长,无法一次读取,终止配置文件加载")
			return
		}
		//读取文件发生错误
		if err != nil {
			//文件已读取结束,终止程序
			if err == io.EOF {
				err = nil
				break
			} else {
				return
			}
		}
		//去除行文本中的空格
		s := strings.TrimSpace(string(line))
		//忽略以'#'或'//'开头的注释行
		if strings.Index(s, "#") == 0 || strings.Index(s, "//") == 0 {
			continue
		}
		//找到行中第一个'='的位值
		index := strings.Index(s, "=")
		//如果行中不包含'=',忽略
		if index < 0 {
			continue
		}
		//把'='左侧的值去掉首尾的空格,作为key值
		first := strings.TrimSpace(s[:index])
		//key为空的忽略掉
		if len(first) == 0 {
			continue
		}
		//把'='右侧的值去掉首尾的空格,作为value值
		second := strings.TrimSpace(s[index+1:])
		//value为空的忽略掉
		if len(second) == 0 {
			continue
		}
		//把正常的读到的key和value值放入map变量存储
		kv[first] = second
	}
	//配置文件读取完成后对其中的变量值进行替换
	replaceVar(kv)
	return
}

//替换配置文件项中的变量值 变量格式为 {{VAR-NAME}}，每行只支持一个变量
func replaceVar(m map[string]string) {
	for k, v := range m {
		if strings.Contains(v, varPrefix) && strings.Contains(v, varSuffix) {
			start := strings.Index(v, varPrefix)
			end := strings.Index(v, varSuffix)
			kv := strings.TrimSpace(v[start+2 : end])
			//如果变量值存在,进行替换,不存在放弃替换
			if _, ok := m[kv]; ok {
				m[k] = v[:start] + m[kv] + v[end+2:]
			}
		}
	}
}
