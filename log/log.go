// Package log CopyRight:2020
// 作者:执着是因为无知
// 1.默认使用时只输出到控制台
// 2.通过SetLogToFile()方法,设置AppName(应用名称),NodeName(节点名称),Dir(日志存放目录),Size(日志文件大小限制),Count(日志文件数量)和Level(日志级别),记录日志到文件
// 3.日志名称为AppName@NodeName.log
package log

import (
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"sync"
)

// XLog 是一个自定义的文件日志输出工具
type XLog struct {
	AppName  string      // 应用标识
	NodeName string      // 节点标识
	Dir      string      // 存储路径
	Size     int64       // 单个日志文件最大大小，单位 M
	Count    int         // 最大日志文件保留数量
	Level    XLogLevel   // 日志级别
	_logger  *log.Logger // 日志记录指针
	_fp      *os.File    // 日志文件指针
	_lock    sync.Mutex  // 锁
	_logfile string      // 日志文件名
	_trace   bool        // 是否开启trace
}

// XLogLevel 日志级别
type XLogLevel uint8

// 日志级别
const (
	FATAL XLogLevel = 1
	ERROR XLogLevel = 2
	WARN  XLogLevel = 3
	INFO  XLogLevel = 4
	DEBUG XLogLevel = 5
	TRACE XLogLevel = 6
)

var _xlog *XLog
var _once sync.Once

// 默认初始化日志模块，只输出到控制台，TRACE级别
func init() {
	_once.Do(func() {
		// 初始化 _xlog对象的默认值
		_xlog = &XLog{
			Level:   TRACE, // 默认记录到Trace级别
			_logger: log.New(os.Stdout, "", log.Ldate|log.Ltime|log.Lshortfile),
		}
		writers := []io.Writer{ // 设置日志输出到控制台
			os.Stdout,
		}
		_xlog._logger = log.New(io.MultiWriter(writers...), "", log.Ldate|log.Ltime|log.Lshortfile)
	})
}

// SetLogToFile 设置日志文件输出
func SetLogToFile(appName string, nodeName string, dir string, size int64, count int, level XLogLevel) {
	trace("[SetLogToFile()]设置日志文件输出开始")
	// 为_xlog对象赋值
	_xlog.AppName = appName   // 默认应用名称
	_xlog.NodeName = nodeName // 默认节点名称
	_xlog.Dir = dir           // 默认当前logs文件夹
	_xlog.Size = size << 20   // 默认一个日志文件1g大小
	_xlog.Count = count       // 默认保存3个日志文件
	_xlog.Level = level       // 默认记录到Trace级别
	_xlog._logfile = fmt.Sprintf("%s/%s@%s.log", _xlog.Dir, _xlog.AppName, _xlog.NodeName)
	trace(fmt.Sprintf("[SetLogToFile()]_xlog=%v", _xlog))
	// 如果日志文件存放目录不存在,创建目录结构
	_, err := os.Stat(_xlog.Dir)
	if err != nil && os.IsNotExist(err) {
		trace("[SetLogToFile()]日志文件存储目录不存在,开始创建目录")
		if err = os.MkdirAll(_xlog.Dir, os.ModePerm); err != nil {
			log.Println("[XLog Error][SetLogToFile()]创建日志文件存储目录失败:", err)
		}
		trace("[SetLogToFile()]创建日志文件存储目录完成")
	}
	// 打开日志文件
	trace("[SetLogToFile()]打开日志文件")
	if f, err := os.OpenFile(_xlog._logfile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666); err != nil {
		log.Println("[XLog Error][SetLogToFile()]打开日志文件失败:", err)
	} else {
		_xlog._fp = f
		trace("[SetLogToFile()]打开日志文件完成")
	}
	trace("[SetLogToFile()]设置日志同时输出到文件和控制台")
	writers := []io.Writer{ // 设置日志输出到文件和控制台
		_xlog._fp,
		os.Stdout,
	}
	_xlog._logger = log.New(io.MultiWriter(writers...), "", log.Ldate|log.Ltime|log.Lshortfile)
	trace("[SetLogToFile()]设置日志文件输出完成")
}

// Rotated 异步处理日志文件，超出大小时进行轮换
func rotated() {
	trace("[rotated()]日志文件循环,开始执行")
	// 未设置日志输出到文件
	if _xlog._logfile == "" {
		trace("[rotated()]未设置日志输出到文件,终止执行")
		return
	}
	// 获取日志文件信息
	trace("[rotated()]获取日志文件信息")
	fs, e := _xlog._fp.Stat()
	if e != nil {
		fmt.Println("[rotated()]获取日志文件信息失败:", e)
		return
	}
	trace("[rotated()]获取日志文件信息完成")
	// 文件超出最大大小限制，进行重命名处理
	if fs.Size() > _xlog.Size {
		trace(fmt.Sprintf("[rotated()]限制大小%v,当前日志文件大小%v,开始更换日志文件", _xlog.Size, fs.Size()))
		for i := _xlog.Count - 1; i > 0; i-- {
			logbak := fmt.Sprintf("%s-%d", _xlog._logfile, i)
			_, err := os.Stat(logbak)
			// 判断备份文件是否存在
			if err == nil || os.IsExist(err) {
				if i == _xlog.Count-1 {
					// 删除最老的文件
					trace(fmt.Sprintf("[rotated()]删除历史日志文件%v", logbak))
					if e := os.Remove(logbak); e != nil {
						log.Printf("[XLog Error][rotated()]删除历史日志文件%v失败:%v\n", logbak, e)
						return
					}
					trace(fmt.Sprintf("[rotated()]删除历史日志文件%v完成", logbak))
				} else {
					// 重命名当前文件为文件名+1
					lognew := fmt.Sprintf("%s-%d", _xlog._logfile, i+1)
					trace(fmt.Sprintf("[rotated()]历史日志文件%v,重命名为%v", logbak, lognew))
					if e := os.Rename(logbak, lognew); e != nil {
						log.Printf("[XLog Error][rotated()]历史日志文件%v,重命名为%v,失败:%v\n", logbak, lognew, e)
						return
					}
					trace(fmt.Sprintf("[rotated()]历史日志文件%v,重命名为%v完成", logbak, lognew))
				}
			}
		}
		defer _xlog._lock.Unlock()
		_xlog._lock.Lock()
		// 把当前文件复制为备份文件，并重建日志输出文件指针和日志记录器指针
		trace("[rotated()]关闭当前日志文件")
		if e := _xlog._fp.Close(); e != nil {
			log.Printf("[XLog Error][rotated()]关闭当前日志文件%v失败:%v\n", _xlog._logfile, e)
		}
		trace("[rotated()]关闭当前日志文件完成")
		trace("[rotated()]把当前日志文件重命名为备份文件")
		newbak := fmt.Sprintf("%s-%d", _xlog._logfile, 1)
		if e := os.Rename(_xlog._logfile, newbak); e != nil {
			log.Printf("[XLog Error][rotated()]把当前日志文件%v重命名为备份文件%v:%v\n", _xlog._logfile, newbak, e)
		}
		trace("[rotated()]把当前日志文件重命名为备份文件完成")
		trace("[rotated()]打开新的日志文件")
		f, e := os.OpenFile(_xlog._logfile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
		if e != nil {
			log.Printf("[XLog Error][rotated()]打开新的日志文件%v失败:%v\n", _xlog._logfile, e)
		}
		trace("[rotated()]打开新的日志文件完成")
		_xlog._fp = f
		writers := []io.Writer{ // 设置日志输出到文件和控制台
			_xlog._fp,
			os.Stdout,
		}
		_xlog._logger.SetOutput(io.MultiWriter(writers...))
		trace("[SetLogToFile()]设置日志同时输出到文件和控制台")
	}
	trace(fmt.Sprintf("[rotated()]限制大小%v,当前日志文件大小%v,无需更换日志文件,执行结束", _xlog.Size, fs.Size()))
}
func (xlog *XLog) log(level XLogLevel, v ...interface{}) {
	if level > xlog.Level {
		return
	}
	v1 := make([]interface{}, len(v)+1)
	v1[0] = level2string(level)
	copy(v1[1:], v)
	xlog._logger.Output(3, fmt.Sprintln(v1...))
	rotated()
}
func (xlog *XLog) logf(level XLogLevel, format string, v ...interface{}) {
	if level > xlog.Level {
		return
	}
	xlog._logger.Output(3, level2string(level)+fmt.Sprintf(format, v...))
	rotated()
}

// 输出trace信息
func trace(str string) {
	if _xlog._trace {
		log.Println("[XLog Trace]" + str)
	}
}

//level2string 把 XLogLevel 转为 string
func level2string(l XLogLevel) string {
	switch l {
	case FATAL:
		return "[Fatal]"
	case ERROR:
		return "[Error]"
	case WARN:
		return "[Warn]"
	case INFO:
		return "[Info]"
	case DEBUG:
		return "[Debug]"
	case TRACE:
		return "[Trace]"
	}
	return "[Uknow]"
}

// ToXLogLevel 把字符串转换为 Level,默认为 TRACE
func ToXLogLevel(s string) (logLevel XLogLevel) {
	s = strings.TrimSpace(s)
	switch strings.ToUpper(s) {
	case "TRACE":
		logLevel = TRACE
	case "DEBUG":
		logLevel = DEBUG
	case "INFO":
		logLevel = INFO
	case "WARN":
		logLevel = WARN
	case "WARNING":
		logLevel = WARN
	case "ERROR":
		logLevel = ERROR
	case "FATAL":
		logLevel = FATAL
	default:
		logLevel = TRACE
	}
	return
}

// Close 释放日志资源
func Close() {
	if _xlog._fp != nil {
		err := _xlog._fp.Close()
		if err != nil {
			log.Println("[XLog Error][Close()]关闭日志文件失败:", err)
		} else {
			if _xlog._trace {
				trace("[Close()]日志文件已关闭")
			}
		}
	}
}

// SetAppName 设置应用标识
func SetAppName(appName string) {
	_xlog.AppName = appName
}

// GetAppName 返回应用标识
func GetAppName() string {
	return _xlog.AppName
}

// SetNodeName 设置节点标识
func SetNodeName(nodeName string) {
	_xlog.NodeName = nodeName
}

// GetNodeName 返回节点标识
func GetNodeName() string {
	return _xlog.NodeName
}

// SetLogDir 设置日志文件存储路径
func SetLogDir(dir string) {
	_xlog.Dir = dir
}

// GetLogDir 返回日志文件存储路径
func GetLogDir() string {
	return _xlog.Dir
}

// SetSize 设置单个日志文件最大尺寸,单位 M
func SetSize(size int64) {
	_xlog.Size = size
}

// GetSize 返回单个日志文件最大尺寸,单位 M
func GetSize() int64 {
	return _xlog.Size
}

// SetCount 设置日志文件最大保留数量
func SetCount(count int) {
	_xlog.Count = count
}

// GetCount 设置日志文件最大保留数量
func GetCount() int {
	return _xlog.Count
}

// SetLevel 设置日志文件最大保留数量
func SetLevel(level XLogLevel) {
	_xlog.Level = level
}

// GetLevel 设置日志文件最大保留数量
func GetLevel() string {
	return level2string(_xlog.Level)
}

// OpenTrace 开启日志trace
func OpenTrace() {
	_xlog._trace = true
}

// CloseTrace 关闭日志trace
func CloseTrace() {
	_xlog._trace = false
}

// Fatal 记录 FATAL 级别的日志，记录完成后程序会退出
func Fatal(v ...interface{}) {
	_xlog.log(FATAL, v...)
	os.Exit(-1)
}

// Fatalf 记录 FATAL 级别的日志，记录完成后程序会退出
func Fatalf(format string, v ...interface{}) {
	_xlog.logf(FATAL, format, v...)
	os.Exit(-1)
}

// Error 记录 ERROR 级别的日志
func Error(v ...interface{}) {
	_xlog.log(ERROR, v...)
}

// Errorf 记录 ERROR 级别的日志
func Errorf(format string, v ...interface{}) {
	_xlog.logf(ERROR, format, v...)
}

// Warn 记录 WARN 级别的日志
func Warn(v ...interface{}) {
	_xlog.log(WARN, v...)
}

// Warnf 记录 WARN 级别的日志
func Warnf(format string, v ...interface{}) {
	_xlog.logf(WARN, format, v...)
}

// Info 记录 INFO 级别的日志
func Info(v ...interface{}) {
	_xlog.log(INFO, v...)
}

// Infof 记录 INFO 级别的日志
func Infof(format string, v ...interface{}) {
	_xlog.logf(INFO, format, v...)
}

// Debug 记录 DEBUG 级别的日志
func Debug(v ...interface{}) {
	_xlog.log(DEBUG, v...)
}

// Debugf 记录 DEBUG 级别的日志
func Debugf(format string, v ...interface{}) {
	_xlog.logf(DEBUG, format, v...)
}

// Trace 记录 TRACE 级别的日志
func Trace(v ...interface{}) {
	_xlog.log(TRACE, v...)
}

// Tracef 记录 TRACE 级别的日志
func Tracef(format string, v ...interface{}) {
	_xlog.logf(TRACE, format, v...)
}
