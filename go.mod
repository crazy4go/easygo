module gitee.com/crazy4go/easygo

go 1.14

require (
	github.com/go-sql-driver/mysql v1.5.0
	xorm.io/xorm v1.0.2
)
