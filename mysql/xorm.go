package mysql

import (
	"fmt"
	"os"

	// 导入mysql数据库官方驱动
	_ "github.com/go-sql-driver/mysql"
	"xorm.io/xorm"
	"xorm.io/xorm/names"
)

// InitXormEngine 初始化 Xorm Engine
func InitXormEngine(url string, maxOpenConns int, maxIdleConns int, prefix string) (engine *xorm.Engine, err error) {
	engine, err = xorm.NewEngine("mysql", url)
	if err != nil {
		fmt.Print(err)
		os.Exit(0)
	}
	err = engine.Ping() // 测试数据库连接可用性
	if err != nil {
		fmt.Print(err)
		os.Exit(0)
	}
	engine.SetMaxIdleConns(maxIdleConns)
	engine.SetMaxOpenConns(maxOpenConns)
	engine.SetTableMapper(names.NewPrefixMapper(names.SnakeMapper{}, prefix))
	return
}
