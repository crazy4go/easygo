package mysql

// 在不使用 ORM 的情况下封装的简易数据库操作框架
// 避免 ORM 框架带来的性能损耗
import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"gitee.com/crazy4go/easygo/log"

	// 导入mysql数据库官方驱动
	_ "github.com/go-sql-driver/mysql"
)

// MySQL 操作封装
type MySQL struct {
	url          string  // 数据库连接
	maxOpenConns int     // 最大打开连接
	maxIdleConns int     // 最大空闲连接
	openConns    int     // 打开连接数
	idleConns    int     // 空闲连接数
	inuseConns   int     // 占用连接数
	db           *sql.DB // 数据库会话
	showTrace    bool    // 是否开启trace
}

// InitMySQL 初始化mysql数据库链接
// 发生错误会中断程序运行
func InitMySQL(url string, maxOpenConns int, maxIdleConns int) (mysql *MySQL, err error) {
	// 构造MySQL结构体
	mysql.url = strings.TrimSpace(url)
	mysql.maxOpenConns = maxOpenConns
	mysql.maxIdleConns = maxIdleConns
	if mysql.db, err = sql.Open("mysql", mysql.url); err != nil {
		return
	}
	mysql.db.SetMaxOpenConns(mysql.maxOpenConns)
	mysql.db.SetMaxIdleConns(mysql.maxIdleConns)
	// 数据库连接测试,因 sql.open 并未创建数据库连接，不能保证可用性
	if err = mysql.db.Ping(); err != nil {
		return
	}
	return
}

// State 获取连接池运行状况,并赋值到mysql对象
func (mysql *MySQL) State() {
	state := mysql.db.Stats()
	mysql.idleConns = state.Idle
	mysql.openConns = state.OpenConnections
	mysql.inuseConns = state.InUse
}

// Query 执行查询操作，返回多行的查询操作
func (mysql *MySQL) Query(querysql string, args ...interface{}) ([]map[string]interface{}, error) {
	list := []map[string]interface{}{}
	mysql.trace(generateSQL(querysql, args...))
	var rows *sql.Rows
	var err error
	rows, err = mysql.db.Query(querysql, args...)
	defer rows.Close()
	if err != nil {
		log.Error(err)
		return list, err
	}
	var cols []string
	cols, err = rows.Columns()
	if err != nil {
		log.Error(err)
		return list, err
	}
	row := []interface{}{}
	for i := 0; i < len(cols); i++ {
		row = append(row, new(string))
	}
	var count int
	for rows.Next() {
		if err = rows.Scan(row...); err != nil {
			log.Error(err)
			return list, err
		}
		record := map[string]interface{}{}
		for seq, colName := range cols {
			record[colName] = row[seq]
		}
		list = append(list, record)
		count++
	}
	if count > 100 {
		log.Warnf("SQL[%s] 返回记录 %d 行，超出100条记录\n", generateSQL(querysql, args...), count)
	}
	return list, err
}

// Query2Json 直接把查询结果转为json
func (mysql *MySQL) Query2Json(querysql string, args ...interface{}) (jsonstr string, err error) {
	list, err := mysql.Query(querysql, args...)
	if s, e := json.Marshal(list); e != nil {
		log.Error("构造json串发生错误")
	} else {
		jsonstr = string(s)
	}
	return
}

// Insert 插入记录
func (mysql *MySQL) Insert(insertsql string, args ...interface{}) error {
	mysql.trace(generateSQL(insertsql, args...))
	_, e := mysql.db.Exec(insertsql, args...)
	return e
}

// Delete 删除记录
func (mysql *MySQL) Delete(deletesql string, args ...interface{}) error {
	mysql.trace(generateSQL(deletesql, args...))
	// TODO 如果语句中不包括 WHERE 关键词，输出告警日志
	if !strings.Contains(deletesql, "where") {
		return errors.New("Delete()语句中必须包含 where 条件")
	}
	_, e := mysql.db.Exec(deletesql, args...)
	return e
}

// Update 更新记录
func (mysql *MySQL) Update(updatesql string, args ...interface{}) error {
	mysql.trace(generateSQL(updatesql, args...))
	// TODO 如果语句中不包括 WHERE 关键词，输出告警日志
	if !strings.Contains(strings.ToLower(updatesql), "where") {
		return errors.New("Update语句中必须包含 where 条件")
	}

	_, e := mysql.db.Exec(updatesql, args...)
	return e
}

// Count 查询数量
func (mysql *MySQL) Count(countsql string, args ...interface{}) int {
	count := 0
	mysql.trace(generateSQL(countsql, args...))
	row := mysql.db.QueryRow(countsql, args...)
	if err := row.Scan(&count); err != nil {
		log.Error(err)
	}
	return count
}

// TransBegin 开启事务
func (mysql *MySQL) TransBegin() (*sql.Tx, error) {
	return mysql.db.Begin()
}

// TransCommit 提交事务
func (mysql *MySQL) TransCommit(tx *sql.Tx) error {
	return tx.Commit()
}

// TransRollback 提交事务
func (mysql *MySQL) TransRollback(tx *sql.Tx) error {
	return tx.Rollback()
}

// Close 销毁数据库连接对象
func (mysql *MySQL) Close() error {
	mysql.trace("数据库连接池已销毁")
	return mysql.db.Close()
}

func generateSQL(querysql string, args ...interface{}) string {
	sql := strings.ReplaceAll(querysql, "?", "'%v'")
	return fmt.Sprintf(sql, args...)
}

func (mysql *MySQL) trace(format string, v ...interface{}) {
	if mysql.showTrace {
		log.Tracef(format, v...)
	}
}
